﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using TaskManager.DAL.Entities.Concrete;

namespace TaskManager.DAL.Contexts
{
    public class TaskContext : DbContext
    {
        static TaskContext()
        {
            Database.SetInitializer<TaskContext>(new TaskDbInitializer());
        }
        public TaskContext() : base("name=TaskDatabase")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(e => e.CreatedTasks)
                .WithRequired(e => e.Creator)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<User>()
                .HasMany(e => e.ContractedTasks)
                .WithRequired(e => e.Contractor)
                .HasForeignKey(e => e.ContractorId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<User>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Creator)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Task>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.TaskId)
                .WillCascadeOnDelete();

            base.OnModelCreating(modelBuilder);
        }
    }

    public class TaskDbInitializer : DropCreateDatabaseIfModelChanges<TaskContext>
    {
        protected override void Seed(TaskContext db)
        {
            db.Users.Add(new User { Id = 10, FirstName = "Джей", SecondaryName = "Ти", Surname = "Миллер" });
            db.Users.Add(new User { Id = 91, FirstName = "Стивен", Surname = "Стэмкос" });
            db.Users.Add(new User { Id = 86, FirstName = "Никита", SecondaryName = "Игоревич", Surname = "Кучеров" });
            db.Users.Add(new User { Id = 0, FirstName = "Джон ", Surname = "Купер" });
            db.Users.Add(new User { Id = 88, FirstName = "Андрей", SecondaryName = "Андреевич", Surname = "Василевский" });

            db.Tasks.Add(new Task { Id = 1, CreatorId = 0, ContractorId = 10, Title = "Выигрывать вбрасывания", Description = "Выигрывать вбрасывания у соперника" });
            db.Tasks.Add(new Task { Id = 2, CreatorId = 0, ContractorId = 91, Title = "Бросать по воротам", Description = "Бросать по воротам и забивать голы" });
            db.Tasks.Add(new Task { Id = 3, CreatorId = 0, ContractorId = 88, Title = "Защищать ворота", Description = "Защищать ворота от бросков соперника" });

            for(var taskId = 1; taskId< 4; taskId++)
            {
                for (var i = 1; i < 4; i++)
                {
                    db.Comments.Add(new Comment { CreatorId = i == 1 ? 0 : i == 2 ? 91 : 88, TaskId = taskId, Description = $"Комментарий #{i} к задаче #{taskId}." });
                }
            }

            db.SaveChanges();
        }
    }
}
