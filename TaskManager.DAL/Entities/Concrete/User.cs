﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.DAL.Entities.Abstract;

namespace TaskManager.DAL.Entities.Concrete
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string SecondaryName { get; set; }
        public string Surname { get; set; }

        public virtual List<Task> CreatedTasks { get; set; }
        public virtual List<Task> ContractedTasks { get; set; }

        public virtual List<Comment> Comments { get; set; }
    }
}
