﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.DAL.Entities.Abstract;

namespace TaskManager.DAL.Entities.Concrete
{
    public class Comment : BaseEntity
    {
        public int TaskId { get; set; }
        public int CreatorId { get; set; }
        public string Description { get; set; }

        public virtual Task Task { get; set; }
        public virtual User Creator { get; set; }
    }
}
