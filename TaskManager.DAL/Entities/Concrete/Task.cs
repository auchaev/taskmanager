﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.DAL.Entities.Abstract;

namespace TaskManager.DAL.Entities.Concrete
{
    public class Task : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int CreatorId { get; set; }
        public int ContractorId { get; set; }

        public virtual User Creator { get; set; }
        public virtual User Contractor { get; set; }

        public virtual List<Comment> Comments { get; set; }
    }
}
