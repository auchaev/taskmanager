﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.DAL.Repositories.Abstract
{
    public interface IRepository<T> where T : class
    {
        T Get(int id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T item);
        void Delete(int id);
    }
}
