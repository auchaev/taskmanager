﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.DAL.Entities.Concrete;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.Repositories.Concrete
{
    public class CommentRepository : IRepository<Comment>
    {
        public void Create(Comment item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comment> Find(Func<Comment, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Comment Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}
