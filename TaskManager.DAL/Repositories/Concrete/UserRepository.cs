﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.DAL.Entities.Concrete;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.Repositories.Concrete
{
    public class UserRepository : IRepository<User>
    {
        public void Create(User item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> Find(Func<User, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public User Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}
