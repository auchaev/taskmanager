﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.DAL.Entities.Concrete;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.Repositories.Concrete
{
    public class TaskRepository : IRepository<Task>
    {
        public void Create(Task item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Task> Find(Func<Task, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Task Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}
